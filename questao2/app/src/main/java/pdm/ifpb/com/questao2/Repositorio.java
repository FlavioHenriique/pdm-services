package pdm.ifpb.com.questao2;

import java.util.Objects;

public class Repositorio {

    private String foto;
    private String nome;
    private String autor;
    private String descricao;

    public Repositorio(String foto, String nome, String autor, String descricao) {
        this.foto = foto;
        this.nome = nome;
        this.autor = autor;
        this.descricao = descricao;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Repositorio that = (Repositorio) o;
        return Objects.equals(foto, that.foto) &&
                Objects.equals(nome, that.nome) &&
                Objects.equals(autor, that.autor) &&
                Objects.equals(descricao, that.descricao);
    }

    @Override
    public int hashCode() {

        return Objects.hash(foto, nome, autor, descricao);
    }

    @Override
    public String toString() {
        return "Repositorio{" +
                "foto='" + foto + '\'' +
                ", nome='" + nome + '\'' +
                ", autor='" + autor + '\'' +
                ", descricao='" + descricao + '\'' +
                '}';
    }
}
