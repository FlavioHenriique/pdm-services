package pdm.ifpb.com.questao2;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public   class MainActivity extends AppCompatActivity {

    public Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Button bt = findViewById(R.id.botao);

        handler = new MyHandler(this);

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText et = findViewById(R.id.user);

                Intent intent = new Intent(MainActivity.this, Rep_Service.class);
                intent.putExtra("nome",et.getText().toString());
                startService(intent);
            }
        });

    }

    private class MyHandler extends Handler {

        private Activity act;

        public MyHandler(Activity act) {
            super();
            this.act = act;
        }

        @Override
        public void handleMessage(Message msg) {
            List<Repositorio> lista = new ArrayList<>();

            ListView lv = findViewById(R.id.repositorios);

            MyAdapter adapter = new MyAdapter(lista, this.act);
            lv.setAdapter(adapter);
        }
    }
}
