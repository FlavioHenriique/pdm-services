package pdm.ifpb.com.questao2;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends BaseAdapter{

    private List<Repositorio> lista;
    private Activity activity;

    public MyAdapter(List<Repositorio> lista, Activity act) {
        this.lista = lista;
        this.activity = act;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = activity.getLayoutInflater().inflate(R.layout.repositorio,
                parent, false);
        Repositorio obj = lista.get(position);

        TextView nome = view.findViewById(R.id.user);
        TextView autor = view.findViewById(R.id.autor);
        TextView descricao = view.findViewById(R.id.descricao);

        nome.setText(obj.getNome());
        autor.setText(obj.getAutor());
        descricao.setText(obj.getDescricao());

        return view;
    }
}
