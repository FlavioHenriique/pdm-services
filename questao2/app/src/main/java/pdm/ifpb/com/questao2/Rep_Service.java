package pdm.ifpb.com.questao2;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Rep_Service extends Service {
    public Rep_Service() {
    }

    @Override
    public IBinder onBind(Intent intent) {

        throw new UnsupportedOperationException("Not yet implemented");

    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {

      new Thread(new Runnable() {
                @Override
                public void run() {

                    URL url = null;

                    String nome = intent.getStringExtra("nome");

                    String link = "https://api.github.com/users/".concat(nome).concat("/repos");

                    try {
                        url = new URL(link);
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.connect();
                        InputStream input = conn.getInputStream();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                        String linha = "";

                        if((linha = reader.readLine()) != null) {
                            System.out.println(linha);
                        }

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }).start();

        return START_STICKY;
    }
}
